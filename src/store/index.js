import Vue from 'vue';
import Vuex from 'vuex';
import shop from '@/api/shop'
import cart from './modules/cart'
import products from './modules/products'

Vue.use(Vuex);

export default new Vuex.Store({
  modules:{
    cart,
    products,
  },

  state: { //data
		
  },
  getters: { //computed property
  	
  },
  mutations: {
  	
  },
  actions: { //methods property
  	//make the call
  	//call mutations
  },
});