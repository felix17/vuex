import shop from '@/api/shop'

export default{
	namespaced: true,

	state: {
		items: [],
		checkoutStatus: null,
	},

	getters: {
		getCartProducts(state, getters, rootState){
			return state.items.map(cartItem => {
				const product = rootState.products.items.find(product => product.id === cartItem.id)
				return {
					title: product.title,
					price: product.price,
					quantity: cartItem.quantity
				}
			})
		},

		cartTotal(state, getters){
			return getters.getCartProducts.reduce((total, product) => total + product.price * product.quantity, 0)
		},
	},

	mutations: {
		pushProductToCart(state, productId){
  		state.items.push({
  			id: productId,
  			quantity: 1
  		})
  	},

  	incrementQuantity(state, cartItem){
  		cartItem.quantity +=1
  	},

  	setCheckoutStatus(state, status){
  		state.checkoutStatus = status
  	},

  	emptyCart(state){
  		state.items = []
  	}
	},

	actions: {
		addProductToCart({state, commit, getters, rootGetters}, product){
  		const cartItem = state.items.find(item => item.id === product.id)
  		if(rootGetters['products/productIsInStock'](product)){
  			if(!cartItem){
  				commit('pushProductToCart', product.id)
  			} else {
  				commit('incrementQuantity', cartItem)
  			}
  			commit('products/decrementInventory', product, {root: true})
  		}
  	},

  	checkout({state, commit}){
  		shop.buyProducts(
  			state.items, 
  			() => {
  				commit('emptyCart')
  				commit('setCheckoutStatus', 'success')
  			},
  			() => {
  				commit('setCheckoutStatus', 'fail')
  			}
  		)
  	},
	}
}